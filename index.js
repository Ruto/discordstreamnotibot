const cron = require('node-cron');

const discord = require('./discord');
const server = require('./server');
const twitch = require('./twitch');

// Schedule hourly job to resubscribe

cron.schedule('0 * * * *', twitch.renewSubscriptions);
