const axios = require('axios').default;
const { base_url, twitch } = require('./config.json');
axios.defaults.headers.common['Client-ID'] = twitch.client_id;

const cacheNames = new Set();

exports.getUserId = async (name) => {
  const response = await axios.get('https://api.twitch.tv/helix/users', {
    params: {
      login: name
    }
  });
  const data = response.data.data;
  if (data.length === 0) return -1;
  return data[0].id;
}

const gameNameCache = {};
exports.getGameName = async (gameId) => {
  if (!gameNameCache[gameId]) {
    const response = await axios.get('https://api.twitch.tv/helix/games', {
      params: {
        id: gameId
      }
    });
    const data = response.data.data;
    if (data.length === 0) return -1;
    gameNameCache[gameId] = data[0].name;
  }
  return gameNameCache[gameId];
}

exports.subscribe = async (name) => {
  name = name.toLowerCase();
  console.log(`[TWITCH] Subscribing to ${name}`);
  cacheNames.add(name);
  return await axios.post('https://api.twitch.tv/helix/webhooks/hub', {
    'hub.topic': `https://api.twitch.tv/helix/streams?user_id=${await this.getUserId(name)}`,
    'hub.mode': 'subscribe',
    'hub.callback': `${base_url}/${name}`,
    'hub.lease_seconds': 3630,
    'hub.secret': 'cake'
  });
}

exports.renewSubscriptions = async () => {
  console.log('[TWITCH] Renewing subscriptions');
  for (let name of cacheNames) {
    await this.subscribe(name);
  }
}
