const express = require('express');
const twitch = require('../../twitch');
const discord = require('../../discord');

const router = express.Router();

// https://dev.twitch.tv/docs/api/webhooks-reference#topic-stream-changed
router.post('/:name', async (req, res) => {
  const name = req.params.name;
  if (req.body.data.length === 0) {
    console.log(`[TWITCH] ${name} stream offline`);
    discord.broadcastOffline(name);
  } else {
    const data = req.body.data[0];
    const game = await twitch.getGameName(data.game_id);
    console.log(`[TWITCH] ${name} is live - ${data.title} - ${game}`);
    discord.broadcastLive({
      name,
      title: data.title,
      game: game,
      viewers: data.viewer_count,
      thumbnail: data.thumbnail_url
    });
  }
  res.sendStatus(200);
});

// https://dev.twitch.tv/docs/api/webhooks-guide#subscriptions
router.get('/:name', (req, res) => {
  console.log(`[TWITCH] Confirming ${req.query['hub.mode']} for ${req.params.name}`);
  const challenge = req.query['hub.challenge'];
  res.send(challenge);
});

module.exports = router;

/*
{
"data": [{
"id": "0123456789",
"user_id": "5678",
"user_name": "wjdtkdqhs",
"game_id": "21779",
"community_ids": [],
"type": "live",
"title": "Best Stream Ever",
"viewer_count": 417,
"started_at": "2017-12-01T10:09:45Z",
"language": "en",
"thumbnail_url": "https://link/to/thumbnail.jpg"
}]
}
*/
