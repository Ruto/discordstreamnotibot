const express = require('express');
const bodyParser = require('body-parser');

const webhookRoute = require('./routes/webhook');

const port = 8090;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/webhook', webhookRoute);

app.listen(port, () => {
  console.log(`TwitchListener starting on port ${port}`)
});

module.exports = app;
