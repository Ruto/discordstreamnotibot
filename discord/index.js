const Discord = require('discord.js');
const { discord } = require('../config.json');

const client = new Discord.Client();

client.once('ready', () => {
	console.log('Discord bot logged in');
});

client.on('message', message => {
	console.log(message.content);
});

client.login(discord.token);

console.log('starting discord');

const messageCache = {};

exports.broadcastOffline = async (name) => {
  console.log(`[DISCORD] ${name} went offline`);
  if (!(name in messageCache)) return;
  for (let message of messageCache[name]) {
    message.delete();
  }
  messageCache[name] = [];
}

exports.broadcastLive = async (data) => {
  const guild = client.guilds.resolve('428654920053555200');
  const channel = guild.channels.resolve('687151981403439114');

  const embed = new Discord.MessageEmbed()
    .setColor('#0099ff')
    .setTitle(`${data.name} is live!`)
    .setThumbnail(data.thumbnail)
    .addField('Game', data.game, true)
    .addField('Title', data.title, true)
    .setTimestamp();
  if (!(data.name in messageCache)) {
    messageCache[data.name] = [];
  }
  if (messageCache[data.name].length === 0) {
    const message = await channel.send(embed);
    messageCache[data.name].push(message);
  } else {
    for (const message of messageCache[data.name]) {
      message.edit(embed);
    }
  }
}
/*discord.broadcastLive({
  name,
  title: data.title,
  game: twitch.getGameName(data.game_id),
  viewers: data.viewer_count,
  thumbnail: data.thumbnail_url
});*/
